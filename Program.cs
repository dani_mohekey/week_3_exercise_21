﻿using System;

namespace week_3_exercise_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var count1 = 5;

            Console.WriteLine("***For Loop.***");

            for (var i = 0; i < count1; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");           
            }

            var count2 = 5;
            var j =0;

            Console.WriteLine("***For a while loop.***");

            while (j < count2)
            {
                var a = j + 1;
                Console.WriteLine($"This is line number {a}");

                j++;
            }

            Console.WriteLine("Task c");

            //in if statement for even number put
            // var a = 2
            //a%2==0
        

        }
    }
}
